#!/usr/bin/env bash

echo ">>> Setting up Vim"

if [[ -z $1 ]]; then
    github_url="https://raw.githubusercontent.com/fideloper/Vaprobash/master"
else
    github_url="$1"
fi

# Create directories needed for some .vimrc settings
mkdir -p /home/fwartner/.vim/backup
mkdir -p /home/fwartner/.vim/swap

# Install Vundle and set owner of .vim files
git clone https://github.com/gmarik/vundle.git /home/fwartner/.vim/bundle/vundle
sudo chown -R fwartner:fwartner /home/fwartner/.vim

# Grab .vimrc and set owner
curl --silent -L $github_url/helpers/vimrc > /home/fwartner/.vimrc
sudo chown fwartner:fwartner /home/fwartner/.vimrc

# Install Vundle Bundles
sudo su - fwartner -c 'vim +BundleInstall +qall'
